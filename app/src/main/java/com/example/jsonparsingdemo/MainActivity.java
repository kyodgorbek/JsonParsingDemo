package com.example.jsonparsingdemo;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.telecom.DisconnectCause;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;

public class MainActivity extends ActionBarActivity {

    private TextView tvData;
    private Connection connection;
    private DisconnectCause disconnect;
    private Connection connect;
    private Reader reader;
    private Readable readLine;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnHit = (Button) findViewById(R.id.btnHit);
        tvData = (TextView) findViewById(R.id.tvJsonItem);

        btnHit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new JsonTask().execute("http://jsonparsing.parseapp.com/jsonData/moviesDemoItem.txt");
            }

            );


        });
}

  public class JsonTask extends AsyncTask<String, String, String> {

      @Override
      protected String doInBackground(String ... params) {

          try {
          URL url = new URL(params[0]);
          connection = (HttpURLConnection) url.openConnection();
          connection.connect();

          InputStream stream = connection.getInputStream();

          reader = new BufferedReader(new InputStreamReader(stream));


          StringBuffer buffer = new StringBuffer();

          String line = "";
          while ((line = reader.readLine()) != null) {
              buffer.append(line);
          }

           String finalJson = buffer.toString();

              JSONObject parentObject = new JSONObject(finalJson);
              JSONArray  parentArray = parentObject.getJSONArray("movies");

              JSONObject finalObject = parentObject.getJSONObject(0);

              String movieName = finalObject.getString("movie");
              int year = finalObject.getInt("year");

              return movieName + " - " + year;

           return buffer.toString();

          } catch (MalformedURLException e) {
              e.printStackTrace();
          }catch (IOException e) {
              e.printStackTrace();
          }catch (JSONException e){
              e.printStackTrace();
          }finally{
              if(connection != null) {
                  connection.disconnect();
              }
              try{

                  if (reader != null) {
                      reader.close();
                  }
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }


      return null;
      }

  }

     @Override
     protected void onPostExecute(String result) {
         super.onPostExecute(result);
         TextView tvData;
         tvData.setText(result);

     }
  }